package com.thattthon.basic_authentication.webService;

import com.thattthon.basic_authentication.model.Employee;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/guest")
@Api(value = "user", description = "Rest API for user operations", tags = "User API")
public class GuestUserController {

    @RequestMapping(value = "/greet/{name}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Display greeting message to non-admin user", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Employee getEmployees()
    {
        return new Employee(1,"Shoe","Male");
    }

}
